#Makefile
CC = gcc
C_FLAGS = -c
OBJECTS = $(add suffix .o, $(SRC)) keeplog.o listlib.o keeplog_helper.o
PGM = keeplog
SRC1 = $(wildcard *.c) keeplog.c keeplog_helper.c listlib.c
SRC = $(patsubst %.c,%,$(SRC1)) keeplog keeplog_helper listlib

$(OBJECTS): %.o: %.c
	$(CC) $(C_FLAGS) $< -o $@
	$(CC) -MM $<> $*.d

-include $(addsuffix .d, $(SRC))

clean:
	@-rm *.o *.d $(PGM)

